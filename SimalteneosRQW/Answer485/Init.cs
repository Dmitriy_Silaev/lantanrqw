﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Xml.Serialization;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;



namespace Answer485
{
    public partial class Form1 : Form
    {
       public void Init()
       {

            for (int i = 0; i < MAXCOM; i++)
            {
                if (i == 0)
                {
                    comobj[i].serialPort = serialPort1;
                    comobj[i].labelName = label1;
                    comobj[i].labelBaudrate = label2;
                    comobj[i].labelTime = label3;
                    comobj[i].labelMaxTime = label4;
                    comobj[i].labelNumReq = label5;
                    comobj[i].labelNumAnsw = label6;
                    comobj[i].comboBox = comboBox1;
                    comobj[i].groupBox = groupBox1;
                    comobj[i].listBox = listBox1;
                    comobj[i].hostnum = 1;
                    comobj[i].portnum = 1;
                }
                if (i == 1)
                {
                    comobj[1].serialPort = serialPort2;
                    comobj[1].labelName = label12;
                    comobj[1].labelBaudrate = label11;
                    comobj[1].labelTime = label10;
                    comobj[1].labelMaxTime = label9;
                    comobj[1].labelNumReq = label8;
                    comobj[1].labelNumAnsw = label7;
                    comobj[1].comboBox = comboBox2;
                    comobj[1].groupBox = groupBox2;
                    comobj[1].listBox = listBox2;
                    comobj[i].hostnum = 1;
                    comobj[i].portnum = 2;
                }
                if (i == 2)
                {
                    comobj[2].serialPort = serialPort3;
                    comobj[2].labelName = label18;
                    comobj[2].labelBaudrate = label17;
                    comobj[2].labelTime = label16;
                    comobj[2].labelMaxTime = label15;
                    comobj[2].labelNumReq = label14;
                    comobj[2].labelNumAnsw = label13;
                    comobj[2].comboBox = comboBox3;
                    comobj[2].groupBox = groupBox3;
                    comobj[2].listBox = listBox3;
                    comobj[i].hostnum = 1;
                    comobj[i].portnum = 3;
                }
                if (i == 3)
                {
                    comobj[3].serialPort = serialPort4;
                    comobj[3].labelName = label24;
                    comobj[3].labelBaudrate = label23;
                    comobj[3].labelTime = label22;
                    comobj[3].labelMaxTime = label21;
                    comobj[3].labelNumReq = label20;
                    comobj[3].labelNumAnsw = label19;
                    comobj[3].comboBox = comboBox4;
                    comobj[3].groupBox = groupBox4;
                    comobj[3].listBox = listBox4;
                    comobj[i].hostnum = 1;
                    comobj[i].portnum = 4;
                }
                ////////////////////////////////////////////////////////////////////
                if (i == 4)
                {
                    comobj[4].serialPort = serialPort5;
                    comobj[4].labelName = label48;
                    comobj[4].labelBaudrate = label47;
                    comobj[4].labelTime = label46;
                    comobj[4].labelMaxTime = label45;
                    comobj[4].labelNumReq = label44;
                    comobj[4].labelNumAnsw = label43;
                    comobj[4].comboBox = comboBox8;
                    comobj[4].groupBox = groupBox8;
                    comobj[4].listBox = listBox8;
                    comobj[i].hostnum = 2;
                    comobj[i].portnum = 1;
                }
                if (i == 5)
                {
                    comobj[5].serialPort = serialPort6;
                    comobj[5].labelName = label42;
                    comobj[5].labelBaudrate = label41;
                    comobj[5].labelTime = label40;
                    comobj[5].labelMaxTime = label39;
                    comobj[5].labelNumReq = label38;
                    comobj[5].labelNumAnsw = label37;
                    comobj[5].comboBox = comboBox7;
                    comobj[5].groupBox = groupBox7;
                    comobj[5].listBox = listBox7;
                    comobj[i].hostnum = 2;
                    comobj[i].portnum = 2;
                }
                if (i == 6)
                {
                    comobj[6].serialPort = serialPort7;
                    comobj[6].labelName = label36;
                    comobj[6].labelBaudrate = label35;
                    comobj[6].labelTime = label34;
                    comobj[6].labelMaxTime = label33;
                    comobj[6].labelNumReq = label32;
                    comobj[6].labelNumAnsw = label31;
                    comobj[6].comboBox = comboBox6;
                    comobj[6].groupBox = groupBox6;
                    comobj[6].listBox = listBox6;
                    comobj[i].hostnum = 2;
                    comobj[i].portnum = 3;
                }
                if (i == 7)
                {
                    comobj[7].serialPort = serialPort8;
                    comobj[7].labelName = label30;
                    comobj[7].labelBaudrate = label29;
                    comobj[7].labelTime = label28;
                    comobj[7].labelMaxTime = label27;
                    comobj[7].labelNumReq = label26;
                    comobj[7].labelNumAnsw = label25;
                    comobj[7].comboBox = comboBox5;
                    comobj[7].groupBox = groupBox5;
                    comobj[7].listBox = listBox5;
                    comobj[i].hostnum = 2;
                    comobj[i].portnum = 4;
                }
                /////////////////////////////////////////////////////////////////////////
                if (i == 8)
                {
                    comobj[8].serialPort = serialPort9;
                    comobj[8].labelName = label72;
                    comobj[8].labelBaudrate = label71;
                    comobj[8].labelTime = label70;
                    comobj[8].labelMaxTime = label69;
                    comobj[8].labelNumReq = label68;
                    comobj[8].labelNumAnsw = label67;
                    comobj[8].comboBox = comboBox12;
                    comobj[8].groupBox = groupBox12;
                    comobj[8].listBox = listBox12;
                    comobj[i].hostnum = 3;
                    comobj[i].portnum = 1;
                }
                if (i == 9)
                {
                    comobj[9].serialPort = serialPort10;
                    comobj[9].labelName = label66;
                    comobj[9].labelBaudrate = label65;
                    comobj[9].labelTime = label64;
                    comobj[9].labelMaxTime = label63;
                    comobj[9].labelNumReq = label62;
                    comobj[9].labelNumAnsw = label61;
                    comobj[9].comboBox = comboBox11;
                    comobj[9].groupBox = groupBox11;
                    comobj[9].listBox = listBox11;
                    comobj[i].hostnum = 3;
                    comobj[i].portnum = 2;
                }
                if (i == 10)
                {
                    comobj[10].serialPort = serialPort11;
                    comobj[10].labelName = label60;
                    comobj[10].labelBaudrate = label59;
                    comobj[10].labelTime = label58;
                    comobj[10].labelMaxTime = label57;
                    comobj[10].labelNumReq = label56;
                    comobj[10].labelNumAnsw = label55;
                    comobj[10].comboBox = comboBox10;
                    comobj[10].groupBox = groupBox10;
                    comobj[10].listBox = listBox10;
                    comobj[i].hostnum = 3;
                    comobj[i].portnum = 3;
                }
                if (i == 11)
                {
                    comobj[11].serialPort = serialPort12;
                    comobj[11].labelName = label54;
                    comobj[11].labelBaudrate = label53;
                    comobj[11].labelTime = label52;
                    comobj[11].labelMaxTime = label51;
                    comobj[11].labelNumReq = label50;
                    comobj[11].labelNumAnsw = label49;
                    comobj[11].comboBox = comboBox9;
                    comobj[11].groupBox = groupBox9;
                    comobj[11].listBox = listBox9;
                    comobj[i].hostnum = 3;
                    comobj[i].portnum = 4;
                }
                ////////////////////////////////////////////////////////////////////////////////////////////////

                if (i == 12)
                {
                    comobj[12].serialPort = serialPort13;
                    comobj[12].labelName = label96;
                    comobj[12].labelBaudrate = label95;
                    comobj[12].labelTime = label94;
                    comobj[12].labelMaxTime = label93;
                    comobj[12].labelNumReq = label92;
                    comobj[12].labelNumAnsw = label91;
                    comobj[12].comboBox = comboBox16;
                    comobj[12].groupBox = groupBox16;
                    comobj[12].listBox = listBox16;
                    comobj[i].hostnum = 4;
                    comobj[i].portnum = 1;
                }
                if (i == 13)
                {
                    comobj[13].serialPort = serialPort14;
                    comobj[13].labelName = label90;
                    comobj[13].labelBaudrate = label89;
                    comobj[13].labelTime = label88;
                    comobj[13].labelMaxTime = label87;
                    comobj[13].labelNumReq = label86;
                    comobj[13].labelNumAnsw = label85;
                    comobj[13].comboBox = comboBox15;
                    comobj[13].groupBox = groupBox15;
                    comobj[13].listBox = listBox15;
                    comobj[i].hostnum = 4;
                    comobj[i].portnum = 2;
                }
                if (i == 14)
                {
                    comobj[14].serialPort = serialPort15;
                    comobj[14].labelName = label84;
                    comobj[14].labelBaudrate = label83;
                    comobj[14].labelTime = label82;
                    comobj[14].labelMaxTime = label81;
                    comobj[14].labelNumReq = label80;
                    comobj[14].labelNumAnsw = label79;
                    comobj[14].comboBox = comboBox14;
                    comobj[14].groupBox = groupBox14;
                    comobj[14].listBox = listBox14;
                    comobj[i].hostnum = 4;
                    comobj[i].portnum = 3;
                }
                if (i == 15)
                {
                    comobj[15].serialPort = serialPort16;
                    comobj[15].labelName = label78;
                    comobj[15].labelBaudrate = label77;
                    comobj[15].labelTime = label76;
                    comobj[15].labelMaxTime = label75;
                    comobj[15].labelNumReq = label74;
                    comobj[15].labelNumAnsw = label73;
                    comobj[15].comboBox = comboBox13;
                    comobj[15].groupBox = groupBox13;
                    comobj[15].listBox = listBox13;
                    comobj[i].hostnum = 4;
                    comobj[i].portnum = 4;
                }
                ////////////////////////////////////////////////////////////////////////////////////////////////

                if (i == 16)
                {
                    comobj[16].serialPort = serialPort17;
                    comobj[16].labelName = label120;
                    comobj[16].labelBaudrate = label119;
                    comobj[16].labelTime = label118;
                    comobj[16].labelMaxTime = label117;
                    comobj[16].labelNumReq = label116;
                    comobj[16].labelNumAnsw = label115;
                    comobj[16].comboBox = comboBox20;
                    comobj[16].groupBox = groupBox20;
                    comobj[16].listBox = listBox20;
                    comobj[i].hostnum = 5;
                    comobj[i].portnum = 1;
                }
                if (i == 17)
                {
                    comobj[17].serialPort = serialPort18;
                    comobj[17].labelName = label114;
                    comobj[17].labelBaudrate = label113;
                    comobj[17].labelTime = label112;
                    comobj[17].labelMaxTime = label111;
                    comobj[17].labelNumReq = label110;
                    comobj[17].labelNumAnsw = label109;
                    comobj[17].comboBox = comboBox19;
                    comobj[17].groupBox = groupBox19;
                    comobj[17].listBox = listBox19;
                    comobj[i].hostnum = 5;
                    comobj[i].portnum = 2;
                }
                if (i == 18)
                {
                    comobj[18].serialPort = serialPort19;
                    comobj[18].labelName = label108;
                    comobj[18].labelBaudrate = label107;
                    comobj[18].labelTime = label106;
                    comobj[18].labelMaxTime = label105;
                    comobj[18].labelNumReq = label104;
                    comobj[18].labelNumAnsw = label103;
                    comobj[18].comboBox = comboBox18;
                    comobj[18].groupBox = groupBox18;
                    comobj[18].listBox = listBox18;
                    comobj[i].hostnum = 5;
                    comobj[i].portnum = 3;
                }
                if (i == 19)
                {
                    comobj[19].serialPort = serialPort20;
                    comobj[19].labelName = label102;
                    comobj[19].labelBaudrate = label101;
                    comobj[19].labelTime = label100;
                    comobj[19].labelMaxTime = label99;
                    comobj[19].labelNumReq = label98;
                    comobj[19].labelNumAnsw = label97;
                    comobj[19].comboBox = comboBox17;
                    comobj[19].groupBox = groupBox17;
                    comobj[19].listBox = listBox17;
                    comobj[i].hostnum = 5;
                    comobj[i].portnum = 4;
                }
            }
        }

    }
}