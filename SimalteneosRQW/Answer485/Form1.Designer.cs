﻿namespace Answer485
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.listBox3 = new System.Windows.Forms.ListBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.listBox4 = new System.Windows.Forms.ListBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.serialPort2 = new System.IO.Ports.SerialPort(this.components);
            this.serialPort3 = new System.IO.Ports.SerialPort(this.components);
            this.serialPort4 = new System.IO.Ports.SerialPort(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.listBox5 = new System.Windows.Forms.ListBox();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.listBox6 = new System.Windows.Forms.ListBox();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.listBox7 = new System.Windows.Forms.ListBox();
            this.comboBox7 = new System.Windows.Forms.ComboBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.listBox8 = new System.Windows.Forms.ListBox();
            this.comboBox8 = new System.Windows.Forms.ComboBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.listBox9 = new System.Windows.Forms.ListBox();
            this.comboBox9 = new System.Windows.Forms.ComboBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.listBox10 = new System.Windows.Forms.ListBox();
            this.comboBox10 = new System.Windows.Forms.ComboBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.button11 = new System.Windows.Forms.Button();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.listBox11 = new System.Windows.Forms.ListBox();
            this.comboBox11 = new System.Windows.Forms.ComboBox();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.button12 = new System.Windows.Forms.Button();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.listBox12 = new System.Windows.Forms.ListBox();
            this.comboBox12 = new System.Windows.Forms.ComboBox();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.button13 = new System.Windows.Forms.Button();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.listBox13 = new System.Windows.Forms.ListBox();
            this.comboBox13 = new System.Windows.Forms.ComboBox();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.button14 = new System.Windows.Forms.Button();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.listBox14 = new System.Windows.Forms.ListBox();
            this.comboBox14 = new System.Windows.Forms.ComboBox();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.button15 = new System.Windows.Forms.Button();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.listBox15 = new System.Windows.Forms.ListBox();
            this.comboBox15 = new System.Windows.Forms.ComboBox();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.button16 = new System.Windows.Forms.Button();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.label95 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.listBox16 = new System.Windows.Forms.ListBox();
            this.comboBox16 = new System.Windows.Forms.ComboBox();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.button17 = new System.Windows.Forms.Button();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.listBox17 = new System.Windows.Forms.ListBox();
            this.comboBox17 = new System.Windows.Forms.ComboBox();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.button18 = new System.Windows.Forms.Button();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.listBox18 = new System.Windows.Forms.ListBox();
            this.comboBox18 = new System.Windows.Forms.ComboBox();
            this.label109 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.button19 = new System.Windows.Forms.Button();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.listBox19 = new System.Windows.Forms.ListBox();
            this.comboBox19 = new System.Windows.Forms.ComboBox();
            this.label115 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.button20 = new System.Windows.Forms.Button();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.label119 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.listBox20 = new System.Windows.Forms.ListBox();
            this.comboBox20 = new System.Windows.Forms.ComboBox();
            this.serialPort5 = new System.IO.Ports.SerialPort(this.components);
            this.serialPort6 = new System.IO.Ports.SerialPort(this.components);
            this.serialPort7 = new System.IO.Ports.SerialPort(this.components);
            this.serialPort8 = new System.IO.Ports.SerialPort(this.components);
            this.serialPort9 = new System.IO.Ports.SerialPort(this.components);
            this.serialPort10 = new System.IO.Ports.SerialPort(this.components);
            this.serialPort11 = new System.IO.Ports.SerialPort(this.components);
            this.serialPort12 = new System.IO.Ports.SerialPort(this.components);
            this.serialPort13 = new System.IO.Ports.SerialPort(this.components);
            this.serialPort14 = new System.IO.Ports.SerialPort(this.components);
            this.serialPort15 = new System.IO.Ports.SerialPort(this.components);
            this.serialPort16 = new System.IO.Ports.SerialPort(this.components);
            this.serialPort17 = new System.IO.Ports.SerialPort(this.components);
            this.serialPort18 = new System.IO.Ports.SerialPort(this.components);
            this.serialPort19 = new System.IO.Ports.SerialPort(this.components);
            this.serialPort20 = new System.IO.Ports.SerialPort(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.SuspendLayout();
            // 
            // serialPort1
            // 
            this.serialPort1.ReadTimeout = 10;
            this.serialPort1.WriteTimeout = 500;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(16, 19);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(83, 43);
            this.listBox1.TabIndex = 0;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "9600",
            "19200",
            "115200"});
            this.comboBox1.Location = new System.Drawing.Point(117, 19);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(99, 21);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.listBox1);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(242, 71);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(180, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "label2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(114, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "label1";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(28, 89);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(83, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(126, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "label3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(126, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "label4";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(192, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "label5";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(192, 118);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "label6";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(438, 118);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "label7";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(438, 89);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "label8";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(372, 118);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "label9";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(372, 89);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "label10";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(274, 89);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(81, 23);
            this.button2.TabIndex = 9;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.listBox2);
            this.groupBox2.Controls.Add(this.comboBox2);
            this.groupBox2.Location = new System.Drawing.Point(258, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(240, 71);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBox2";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(180, 49);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "label11";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(114, 49);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "label12";
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(16, 19);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(84, 43);
            this.listBox2.TabIndex = 0;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "9600",
            "19200",
            "115200"});
            this.comboBox2.Location = new System.Drawing.Point(117, 19);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(99, 21);
            this.comboBox2.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(684, 118);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 13);
            this.label13.TabIndex = 19;
            this.label13.Text = "label13";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(684, 89);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 13);
            this.label14.TabIndex = 18;
            this.label14.Text = "label14";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(623, 118);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 13);
            this.label15.TabIndex = 17;
            this.label15.Text = "label15";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(623, 89);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 13);
            this.label16.TabIndex = 16;
            this.label16.Text = "label16";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(520, 89);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(84, 23);
            this.button3.TabIndex = 15;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.listBox3);
            this.groupBox3.Controls.Add(this.comboBox3);
            this.groupBox3.Location = new System.Drawing.Point(504, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(240, 71);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "groupBox3";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(180, 49);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(41, 13);
            this.label17.TabIndex = 4;
            this.label17.Text = "label17";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(119, 49);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 13);
            this.label18.TabIndex = 4;
            this.label18.Text = "label18";
            // 
            // listBox3
            // 
            this.listBox3.FormattingEnabled = true;
            this.listBox3.Location = new System.Drawing.Point(16, 19);
            this.listBox3.Name = "listBox3";
            this.listBox3.Size = new System.Drawing.Size(84, 43);
            this.listBox3.TabIndex = 0;
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "9600",
            "19200",
            "115200"});
            this.comboBox3.Location = new System.Drawing.Point(122, 19);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(99, 21);
            this.comboBox3.TabIndex = 1;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(930, 118);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(41, 13);
            this.label19.TabIndex = 25;
            this.label19.Text = "label19";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(930, 89);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(41, 13);
            this.label20.TabIndex = 24;
            this.label20.Text = "label20";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(869, 118);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(41, 13);
            this.label21.TabIndex = 23;
            this.label21.Text = "label21";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(869, 89);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 13);
            this.label22.TabIndex = 22;
            this.label22.Text = "label22";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(766, 89);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(84, 23);
            this.button4.TabIndex = 21;
            this.button4.Text = "button4";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.listBox4);
            this.groupBox4.Controls.Add(this.comboBox4);
            this.groupBox4.Location = new System.Drawing.Point(750, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(240, 71);
            this.groupBox4.TabIndex = 20;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "groupBox4";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(180, 49);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(41, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = "label23";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(119, 49);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(41, 13);
            this.label24.TabIndex = 4;
            this.label24.Text = "label24";
            // 
            // listBox4
            // 
            this.listBox4.FormattingEnabled = true;
            this.listBox4.Location = new System.Drawing.Point(16, 19);
            this.listBox4.Name = "listBox4";
            this.listBox4.Size = new System.Drawing.Size(84, 43);
            this.listBox4.TabIndex = 0;
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Items.AddRange(new object[] {
            "9600",
            "19200",
            "115200"});
            this.comboBox4.Location = new System.Drawing.Point(122, 19);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(99, 21);
            this.comboBox4.TabIndex = 1;
            // 
            // serialPort2
            // 
            this.serialPort2.ReadTimeout = 10;
            this.serialPort2.WriteTimeout = 500;
            // 
            // serialPort3
            // 
            this.serialPort3.ReadTimeout = 10;
            this.serialPort3.WriteTimeout = 500;
            // 
            // serialPort4
            // 
            this.serialPort4.ReadTimeout = 10;
            this.serialPort4.WriteTimeout = 500;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(930, 235);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(41, 13);
            this.label25.TabIndex = 49;
            this.label25.Text = "label25";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(930, 207);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(41, 13);
            this.label26.TabIndex = 48;
            this.label26.Text = "label26";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(869, 235);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(41, 13);
            this.label27.TabIndex = 47;
            this.label27.Text = "label27";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(869, 207);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(41, 13);
            this.label28.TabIndex = 46;
            this.label28.Text = "label28";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(766, 210);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(84, 23);
            this.button5.TabIndex = 45;
            this.button5.Text = "button5";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label29);
            this.groupBox5.Controls.Add(this.label30);
            this.groupBox5.Controls.Add(this.listBox5);
            this.groupBox5.Controls.Add(this.comboBox5);
            this.groupBox5.Location = new System.Drawing.Point(750, 136);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(240, 68);
            this.groupBox5.TabIndex = 44;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "groupBox5";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(180, 49);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(41, 13);
            this.label29.TabIndex = 4;
            this.label29.Text = "label29";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(119, 49);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(41, 13);
            this.label30.TabIndex = 4;
            this.label30.Text = "label30";
            // 
            // listBox5
            // 
            this.listBox5.FormattingEnabled = true;
            this.listBox5.Location = new System.Drawing.Point(16, 19);
            this.listBox5.Name = "listBox5";
            this.listBox5.Size = new System.Drawing.Size(84, 43);
            this.listBox5.TabIndex = 0;
            // 
            // comboBox5
            // 
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Items.AddRange(new object[] {
            "9600",
            "19200",
            "115200"});
            this.comboBox5.Location = new System.Drawing.Point(122, 19);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(99, 21);
            this.comboBox5.TabIndex = 1;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(684, 235);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(41, 13);
            this.label31.TabIndex = 43;
            this.label31.Text = "label31";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(684, 207);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(41, 13);
            this.label32.TabIndex = 42;
            this.label32.Text = "label32";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(623, 235);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(41, 13);
            this.label33.TabIndex = 41;
            this.label33.Text = "label33";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(623, 207);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(41, 13);
            this.label34.TabIndex = 40;
            this.label34.Text = "label34";
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(520, 210);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(84, 23);
            this.button6.TabIndex = 39;
            this.button6.Text = "button6";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label35);
            this.groupBox6.Controls.Add(this.label36);
            this.groupBox6.Controls.Add(this.listBox6);
            this.groupBox6.Controls.Add(this.comboBox6);
            this.groupBox6.Location = new System.Drawing.Point(504, 136);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(240, 68);
            this.groupBox6.TabIndex = 38;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "groupBox6";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(180, 49);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(41, 13);
            this.label35.TabIndex = 4;
            this.label35.Text = "label35";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(119, 49);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(41, 13);
            this.label36.TabIndex = 4;
            this.label36.Text = "label36";
            // 
            // listBox6
            // 
            this.listBox6.FormattingEnabled = true;
            this.listBox6.Location = new System.Drawing.Point(16, 19);
            this.listBox6.Name = "listBox6";
            this.listBox6.Size = new System.Drawing.Size(84, 43);
            this.listBox6.TabIndex = 0;
            // 
            // comboBox6
            // 
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.Items.AddRange(new object[] {
            "9600",
            "19200",
            "115200"});
            this.comboBox6.Location = new System.Drawing.Point(122, 19);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(99, 21);
            this.comboBox6.TabIndex = 1;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(438, 235);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(41, 13);
            this.label37.TabIndex = 37;
            this.label37.Text = "label37";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(438, 207);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(41, 13);
            this.label38.TabIndex = 36;
            this.label38.Text = "label38";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(372, 235);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(41, 13);
            this.label39.TabIndex = 35;
            this.label39.Text = "label39";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(372, 207);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(41, 13);
            this.label40.TabIndex = 34;
            this.label40.Text = "label40";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(274, 210);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(81, 23);
            this.button7.TabIndex = 33;
            this.button7.Text = "button7";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label41);
            this.groupBox7.Controls.Add(this.label42);
            this.groupBox7.Controls.Add(this.listBox7);
            this.groupBox7.Controls.Add(this.comboBox7);
            this.groupBox7.Location = new System.Drawing.Point(258, 136);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(240, 68);
            this.groupBox7.TabIndex = 32;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "groupBox7";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(180, 49);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(41, 13);
            this.label41.TabIndex = 4;
            this.label41.Text = "label41";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(114, 49);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(41, 13);
            this.label42.TabIndex = 4;
            this.label42.Text = "label42";
            // 
            // listBox7
            // 
            this.listBox7.FormattingEnabled = true;
            this.listBox7.Location = new System.Drawing.Point(16, 19);
            this.listBox7.Name = "listBox7";
            this.listBox7.Size = new System.Drawing.Size(84, 43);
            this.listBox7.TabIndex = 0;
            // 
            // comboBox7
            // 
            this.comboBox7.FormattingEnabled = true;
            this.comboBox7.Items.AddRange(new object[] {
            "9600",
            "19200",
            "115200"});
            this.comboBox7.Location = new System.Drawing.Point(117, 17);
            this.comboBox7.Name = "comboBox7";
            this.comboBox7.Size = new System.Drawing.Size(99, 21);
            this.comboBox7.TabIndex = 1;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(192, 235);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(41, 13);
            this.label43.TabIndex = 31;
            this.label43.Text = "label43";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(192, 207);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(41, 13);
            this.label44.TabIndex = 30;
            this.label44.Text = "label44";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(126, 235);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(41, 13);
            this.label45.TabIndex = 29;
            this.label45.Text = "label45";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(126, 207);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(41, 13);
            this.label46.TabIndex = 28;
            this.label46.Text = "label46";
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(28, 210);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(83, 23);
            this.button8.TabIndex = 27;
            this.button8.Text = "button8";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label47);
            this.groupBox8.Controls.Add(this.label48);
            this.groupBox8.Controls.Add(this.listBox8);
            this.groupBox8.Controls.Add(this.comboBox8);
            this.groupBox8.Location = new System.Drawing.Point(12, 134);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(240, 70);
            this.groupBox8.TabIndex = 26;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "groupBox8";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(180, 49);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(41, 13);
            this.label47.TabIndex = 4;
            this.label47.Text = "label47";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(114, 49);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(41, 13);
            this.label48.TabIndex = 4;
            this.label48.Text = "label48";
            // 
            // listBox8
            // 
            this.listBox8.FormattingEnabled = true;
            this.listBox8.Location = new System.Drawing.Point(16, 19);
            this.listBox8.Name = "listBox8";
            this.listBox8.Size = new System.Drawing.Size(83, 43);
            this.listBox8.TabIndex = 0;
            // 
            // comboBox8
            // 
            this.comboBox8.FormattingEnabled = true;
            this.comboBox8.Items.AddRange(new object[] {
            "9600",
            "19200",
            "115200"});
            this.comboBox8.Location = new System.Drawing.Point(117, 19);
            this.comboBox8.Name = "comboBox8";
            this.comboBox8.Size = new System.Drawing.Size(99, 21);
            this.comboBox8.TabIndex = 1;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(930, 355);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(41, 13);
            this.label49.TabIndex = 73;
            this.label49.Text = "label49";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(930, 326);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(41, 13);
            this.label50.TabIndex = 72;
            this.label50.Text = "label50";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(869, 355);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(41, 13);
            this.label51.TabIndex = 71;
            this.label51.Text = "label51";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(869, 326);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(41, 13);
            this.label52.TabIndex = 70;
            this.label52.Text = "label52";
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(766, 326);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(84, 23);
            this.button9.TabIndex = 69;
            this.button9.Text = "button9";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label53);
            this.groupBox9.Controls.Add(this.label54);
            this.groupBox9.Controls.Add(this.listBox9);
            this.groupBox9.Controls.Add(this.comboBox9);
            this.groupBox9.Location = new System.Drawing.Point(750, 251);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(240, 69);
            this.groupBox9.TabIndex = 68;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "groupBox9";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(180, 49);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(41, 13);
            this.label53.TabIndex = 4;
            this.label53.Text = "label53";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(119, 49);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(41, 13);
            this.label54.TabIndex = 4;
            this.label54.Text = "label54";
            // 
            // listBox9
            // 
            this.listBox9.FormattingEnabled = true;
            this.listBox9.Location = new System.Drawing.Point(16, 19);
            this.listBox9.Name = "listBox9";
            this.listBox9.Size = new System.Drawing.Size(84, 43);
            this.listBox9.TabIndex = 0;
            // 
            // comboBox9
            // 
            this.comboBox9.FormattingEnabled = true;
            this.comboBox9.Items.AddRange(new object[] {
            "9600",
            "19200",
            "115200"});
            this.comboBox9.Location = new System.Drawing.Point(122, 19);
            this.comboBox9.Name = "comboBox9";
            this.comboBox9.Size = new System.Drawing.Size(99, 21);
            this.comboBox9.TabIndex = 1;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(684, 355);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(41, 13);
            this.label55.TabIndex = 67;
            this.label55.Text = "label55";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(684, 326);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(41, 13);
            this.label56.TabIndex = 66;
            this.label56.Text = "label56";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(623, 355);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(41, 13);
            this.label57.TabIndex = 65;
            this.label57.Text = "label57";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(623, 326);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(41, 13);
            this.label58.TabIndex = 64;
            this.label58.Text = "label58";
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(520, 326);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(84, 23);
            this.button10.TabIndex = 63;
            this.button10.Text = "button10";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label59);
            this.groupBox10.Controls.Add(this.label60);
            this.groupBox10.Controls.Add(this.listBox10);
            this.groupBox10.Controls.Add(this.comboBox10);
            this.groupBox10.Location = new System.Drawing.Point(504, 251);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(240, 69);
            this.groupBox10.TabIndex = 62;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "groupBox10";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(180, 49);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(41, 13);
            this.label59.TabIndex = 4;
            this.label59.Text = "label59";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(119, 49);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(41, 13);
            this.label60.TabIndex = 4;
            this.label60.Text = "label60";
            // 
            // listBox10
            // 
            this.listBox10.FormattingEnabled = true;
            this.listBox10.Location = new System.Drawing.Point(16, 19);
            this.listBox10.Name = "listBox10";
            this.listBox10.Size = new System.Drawing.Size(84, 43);
            this.listBox10.TabIndex = 0;
            // 
            // comboBox10
            // 
            this.comboBox10.FormattingEnabled = true;
            this.comboBox10.Items.AddRange(new object[] {
            "9600",
            "19200",
            "115200"});
            this.comboBox10.Location = new System.Drawing.Point(122, 19);
            this.comboBox10.Name = "comboBox10";
            this.comboBox10.Size = new System.Drawing.Size(99, 21);
            this.comboBox10.TabIndex = 1;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(433, 355);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(41, 13);
            this.label61.TabIndex = 61;
            this.label61.Text = "label61";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(433, 326);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(41, 13);
            this.label62.TabIndex = 60;
            this.label62.Text = "label62";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(372, 355);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(41, 13);
            this.label63.TabIndex = 59;
            this.label63.Text = "label63";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(372, 326);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(41, 13);
            this.label64.TabIndex = 58;
            this.label64.Text = "label64";
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(274, 326);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(84, 23);
            this.button11.TabIndex = 57;
            this.button11.Text = "button11";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label65);
            this.groupBox11.Controls.Add(this.label66);
            this.groupBox11.Controls.Add(this.listBox11);
            this.groupBox11.Controls.Add(this.comboBox11);
            this.groupBox11.Location = new System.Drawing.Point(258, 251);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(240, 69);
            this.groupBox11.TabIndex = 56;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "groupBox11";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(175, 49);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(41, 13);
            this.label65.TabIndex = 4;
            this.label65.Text = "label65";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(114, 49);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(41, 13);
            this.label66.TabIndex = 4;
            this.label66.Text = "label66";
            // 
            // listBox11
            // 
            this.listBox11.FormattingEnabled = true;
            this.listBox11.Location = new System.Drawing.Point(16, 19);
            this.listBox11.Name = "listBox11";
            this.listBox11.Size = new System.Drawing.Size(84, 43);
            this.listBox11.TabIndex = 0;
            // 
            // comboBox11
            // 
            this.comboBox11.FormattingEnabled = true;
            this.comboBox11.Items.AddRange(new object[] {
            "9600",
            "19200",
            "115200"});
            this.comboBox11.Location = new System.Drawing.Point(117, 19);
            this.comboBox11.Name = "comboBox11";
            this.comboBox11.Size = new System.Drawing.Size(99, 21);
            this.comboBox11.TabIndex = 1;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(187, 355);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(41, 13);
            this.label67.TabIndex = 55;
            this.label67.Text = "label67";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(187, 326);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(41, 13);
            this.label68.TabIndex = 54;
            this.label68.Text = "label68";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(126, 355);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(41, 13);
            this.label69.TabIndex = 53;
            this.label69.Text = "label69";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(126, 326);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(41, 13);
            this.label70.TabIndex = 52;
            this.label70.Text = "label70";
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(28, 326);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(83, 23);
            this.button12.TabIndex = 51;
            this.button12.Text = "button12";
            this.button12.UseVisualStyleBackColor = true;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.label71);
            this.groupBox12.Controls.Add(this.label72);
            this.groupBox12.Controls.Add(this.listBox12);
            this.groupBox12.Controls.Add(this.comboBox12);
            this.groupBox12.Location = new System.Drawing.Point(12, 251);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(240, 69);
            this.groupBox12.TabIndex = 50;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "groupBox12";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(175, 49);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(41, 13);
            this.label71.TabIndex = 4;
            this.label71.Text = "label71";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(114, 49);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(41, 13);
            this.label72.TabIndex = 4;
            this.label72.Text = "label72";
            // 
            // listBox12
            // 
            this.listBox12.FormattingEnabled = true;
            this.listBox12.Location = new System.Drawing.Point(16, 19);
            this.listBox12.Name = "listBox12";
            this.listBox12.Size = new System.Drawing.Size(83, 43);
            this.listBox12.TabIndex = 0;
            // 
            // comboBox12
            // 
            this.comboBox12.FormattingEnabled = true;
            this.comboBox12.Items.AddRange(new object[] {
            "9600",
            "19200",
            "115200"});
            this.comboBox12.Location = new System.Drawing.Point(117, 19);
            this.comboBox12.Name = "comboBox12";
            this.comboBox12.Size = new System.Drawing.Size(99, 21);
            this.comboBox12.TabIndex = 1;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(930, 482);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(41, 13);
            this.label73.TabIndex = 97;
            this.label73.Text = "label73";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(930, 449);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(41, 13);
            this.label74.TabIndex = 96;
            this.label74.Text = "label74";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(869, 482);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(41, 13);
            this.label75.TabIndex = 95;
            this.label75.Text = "label75";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(869, 449);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(41, 13);
            this.label76.TabIndex = 94;
            this.label76.Text = "label76";
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(767, 449);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(83, 23);
            this.button13.TabIndex = 93;
            this.button13.Text = "button13";
            this.button13.UseVisualStyleBackColor = true;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.label77);
            this.groupBox13.Controls.Add(this.label78);
            this.groupBox13.Controls.Add(this.listBox13);
            this.groupBox13.Controls.Add(this.comboBox13);
            this.groupBox13.Location = new System.Drawing.Point(751, 371);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(240, 72);
            this.groupBox13.TabIndex = 92;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "groupBox13";
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(179, 49);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(41, 13);
            this.label77.TabIndex = 4;
            this.label77.Text = "label77";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(118, 49);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(41, 13);
            this.label78.TabIndex = 4;
            this.label78.Text = "label78";
            // 
            // listBox13
            // 
            this.listBox13.FormattingEnabled = true;
            this.listBox13.Location = new System.Drawing.Point(16, 19);
            this.listBox13.Name = "listBox13";
            this.listBox13.Size = new System.Drawing.Size(83, 43);
            this.listBox13.TabIndex = 0;
            // 
            // comboBox13
            // 
            this.comboBox13.FormattingEnabled = true;
            this.comboBox13.Items.AddRange(new object[] {
            "9600",
            "19200",
            "115200"});
            this.comboBox13.Location = new System.Drawing.Point(121, 19);
            this.comboBox13.Name = "comboBox13";
            this.comboBox13.Size = new System.Drawing.Size(99, 21);
            this.comboBox13.TabIndex = 1;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(684, 482);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(41, 13);
            this.label79.TabIndex = 91;
            this.label79.Text = "label79";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(684, 449);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(41, 13);
            this.label80.TabIndex = 90;
            this.label80.Text = "label80";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(623, 482);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(41, 13);
            this.label81.TabIndex = 89;
            this.label81.Text = "label81";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(623, 449);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(41, 13);
            this.label82.TabIndex = 88;
            this.label82.Text = "label82";
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(520, 449);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(84, 23);
            this.button14.TabIndex = 87;
            this.button14.Text = "button14";
            this.button14.UseVisualStyleBackColor = true;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.label83);
            this.groupBox14.Controls.Add(this.label84);
            this.groupBox14.Controls.Add(this.listBox14);
            this.groupBox14.Controls.Add(this.comboBox14);
            this.groupBox14.Location = new System.Drawing.Point(504, 371);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(240, 65);
            this.groupBox14.TabIndex = 86;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "groupBox14";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(180, 49);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(41, 13);
            this.label83.TabIndex = 4;
            this.label83.Text = "label83";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(119, 49);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(41, 13);
            this.label84.TabIndex = 4;
            this.label84.Text = "label84";
            // 
            // listBox14
            // 
            this.listBox14.FormattingEnabled = true;
            this.listBox14.Location = new System.Drawing.Point(16, 19);
            this.listBox14.Name = "listBox14";
            this.listBox14.Size = new System.Drawing.Size(84, 43);
            this.listBox14.TabIndex = 0;
            // 
            // comboBox14
            // 
            this.comboBox14.FormattingEnabled = true;
            this.comboBox14.Items.AddRange(new object[] {
            "9600",
            "19200",
            "115200"});
            this.comboBox14.Location = new System.Drawing.Point(122, 19);
            this.comboBox14.Name = "comboBox14";
            this.comboBox14.Size = new System.Drawing.Size(99, 21);
            this.comboBox14.TabIndex = 1;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(433, 482);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(41, 13);
            this.label85.TabIndex = 85;
            this.label85.Text = "label85";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(432, 449);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(41, 13);
            this.label86.TabIndex = 84;
            this.label86.Text = "label86";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(372, 482);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(41, 13);
            this.label87.TabIndex = 83;
            this.label87.Text = "label87";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(372, 449);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(41, 13);
            this.label88.TabIndex = 82;
            this.label88.Text = "label88";
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(274, 449);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(84, 23);
            this.button15.TabIndex = 81;
            this.button15.Text = "button15";
            this.button15.UseVisualStyleBackColor = true;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.label89);
            this.groupBox15.Controls.Add(this.label90);
            this.groupBox15.Controls.Add(this.listBox15);
            this.groupBox15.Controls.Add(this.comboBox15);
            this.groupBox15.Location = new System.Drawing.Point(258, 371);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(240, 65);
            this.groupBox15.TabIndex = 80;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "groupBox15";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(174, 49);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(41, 13);
            this.label89.TabIndex = 4;
            this.label89.Text = "label89";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(114, 49);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(41, 13);
            this.label90.TabIndex = 4;
            this.label90.Text = "label90";
            // 
            // listBox15
            // 
            this.listBox15.FormattingEnabled = true;
            this.listBox15.Location = new System.Drawing.Point(16, 19);
            this.listBox15.Name = "listBox15";
            this.listBox15.Size = new System.Drawing.Size(84, 43);
            this.listBox15.TabIndex = 0;
            // 
            // comboBox15
            // 
            this.comboBox15.FormattingEnabled = true;
            this.comboBox15.Items.AddRange(new object[] {
            "9600",
            "19200",
            "115200"});
            this.comboBox15.Location = new System.Drawing.Point(117, 19);
            this.comboBox15.Name = "comboBox15";
            this.comboBox15.Size = new System.Drawing.Size(99, 21);
            this.comboBox15.TabIndex = 1;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(187, 482);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(41, 13);
            this.label91.TabIndex = 79;
            this.label91.Text = "label91";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(187, 449);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(41, 13);
            this.label92.TabIndex = 78;
            this.label92.Text = "label92";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(126, 482);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(41, 13);
            this.label93.TabIndex = 77;
            this.label93.Text = "label93";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(126, 449);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(41, 13);
            this.label94.TabIndex = 76;
            this.label94.Text = "label94";
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(28, 449);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(83, 23);
            this.button16.TabIndex = 75;
            this.button16.Text = "button16";
            this.button16.UseVisualStyleBackColor = true;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.label95);
            this.groupBox16.Controls.Add(this.label96);
            this.groupBox16.Controls.Add(this.listBox16);
            this.groupBox16.Controls.Add(this.comboBox16);
            this.groupBox16.Location = new System.Drawing.Point(12, 371);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(240, 72);
            this.groupBox16.TabIndex = 74;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "groupBox16";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(175, 49);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(41, 13);
            this.label95.TabIndex = 4;
            this.label95.Text = "label95";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(114, 49);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(41, 13);
            this.label96.TabIndex = 4;
            this.label96.Text = "label96";
            // 
            // listBox16
            // 
            this.listBox16.FormattingEnabled = true;
            this.listBox16.Location = new System.Drawing.Point(16, 19);
            this.listBox16.Name = "listBox16";
            this.listBox16.Size = new System.Drawing.Size(83, 43);
            this.listBox16.TabIndex = 0;
            // 
            // comboBox16
            // 
            this.comboBox16.FormattingEnabled = true;
            this.comboBox16.Items.AddRange(new object[] {
            "9600",
            "19200",
            "115200"});
            this.comboBox16.Location = new System.Drawing.Point(117, 19);
            this.comboBox16.Name = "comboBox16";
            this.comboBox16.Size = new System.Drawing.Size(99, 21);
            this.comboBox16.TabIndex = 1;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(930, 609);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(41, 13);
            this.label97.TabIndex = 121;
            this.label97.Text = "label97";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(930, 577);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(41, 13);
            this.label98.TabIndex = 120;
            this.label98.Text = "label98";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(869, 609);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(41, 13);
            this.label99.TabIndex = 119;
            this.label99.Text = "label99";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(869, 577);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(47, 13);
            this.label100.TabIndex = 118;
            this.label100.Text = "label100";
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(767, 577);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(83, 23);
            this.button17.TabIndex = 117;
            this.button17.Text = "button17";
            this.button17.UseVisualStyleBackColor = true;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.label101);
            this.groupBox17.Controls.Add(this.label102);
            this.groupBox17.Controls.Add(this.listBox17);
            this.groupBox17.Controls.Add(this.comboBox17);
            this.groupBox17.Location = new System.Drawing.Point(751, 498);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(240, 73);
            this.groupBox17.TabIndex = 116;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "groupBox17";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(179, 49);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(47, 13);
            this.label101.TabIndex = 4;
            this.label101.Text = "label101";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(118, 49);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(47, 13);
            this.label102.TabIndex = 4;
            this.label102.Text = "label102";
            // 
            // listBox17
            // 
            this.listBox17.FormattingEnabled = true;
            this.listBox17.Location = new System.Drawing.Point(16, 19);
            this.listBox17.Name = "listBox17";
            this.listBox17.Size = new System.Drawing.Size(83, 43);
            this.listBox17.TabIndex = 0;
            // 
            // comboBox17
            // 
            this.comboBox17.FormattingEnabled = true;
            this.comboBox17.Items.AddRange(new object[] {
            "9600",
            "19200",
            "115200"});
            this.comboBox17.Location = new System.Drawing.Point(121, 19);
            this.comboBox17.Name = "comboBox17";
            this.comboBox17.Size = new System.Drawing.Size(99, 21);
            this.comboBox17.TabIndex = 1;
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(684, 609);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(47, 13);
            this.label103.TabIndex = 115;
            this.label103.Text = "label103";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Location = new System.Drawing.Point(684, 577);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(47, 13);
            this.label104.TabIndex = 114;
            this.label104.Text = "label104";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(623, 609);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(47, 13);
            this.label105.TabIndex = 113;
            this.label105.Text = "label105";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(623, 577);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(47, 13);
            this.label106.TabIndex = 112;
            this.label106.Text = "label106";
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(520, 577);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(84, 23);
            this.button18.TabIndex = 111;
            this.button18.Text = "button18";
            this.button18.UseVisualStyleBackColor = true;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.label107);
            this.groupBox18.Controls.Add(this.label108);
            this.groupBox18.Controls.Add(this.listBox18);
            this.groupBox18.Controls.Add(this.comboBox18);
            this.groupBox18.Location = new System.Drawing.Point(504, 498);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(240, 73);
            this.groupBox18.TabIndex = 110;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "groupBox18";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(180, 49);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(47, 13);
            this.label107.TabIndex = 4;
            this.label107.Text = "label107";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(119, 49);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(47, 13);
            this.label108.TabIndex = 4;
            this.label108.Text = "label108";
            // 
            // listBox18
            // 
            this.listBox18.FormattingEnabled = true;
            this.listBox18.Location = new System.Drawing.Point(16, 19);
            this.listBox18.Name = "listBox18";
            this.listBox18.Size = new System.Drawing.Size(84, 43);
            this.listBox18.TabIndex = 0;
            // 
            // comboBox18
            // 
            this.comboBox18.FormattingEnabled = true;
            this.comboBox18.Items.AddRange(new object[] {
            "9600",
            "19200",
            "115200"});
            this.comboBox18.Location = new System.Drawing.Point(122, 19);
            this.comboBox18.Name = "comboBox18";
            this.comboBox18.Size = new System.Drawing.Size(99, 21);
            this.comboBox18.TabIndex = 1;
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(433, 609);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(47, 13);
            this.label109.TabIndex = 109;
            this.label109.Text = "label109";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Location = new System.Drawing.Point(433, 577);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(47, 13);
            this.label110.TabIndex = 108;
            this.label110.Text = "label110";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(372, 609);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(47, 13);
            this.label111.TabIndex = 107;
            this.label111.Text = "label111";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Location = new System.Drawing.Point(372, 577);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(47, 13);
            this.label112.TabIndex = 106;
            this.label112.Text = "label112";
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(274, 577);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(84, 23);
            this.button19.TabIndex = 105;
            this.button19.Text = "button19";
            this.button19.UseVisualStyleBackColor = true;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.label113);
            this.groupBox19.Controls.Add(this.label114);
            this.groupBox19.Controls.Add(this.listBox19);
            this.groupBox19.Controls.Add(this.comboBox19);
            this.groupBox19.Location = new System.Drawing.Point(258, 498);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(240, 73);
            this.groupBox19.TabIndex = 104;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "groupBox19";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Location = new System.Drawing.Point(175, 49);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(47, 13);
            this.label113.TabIndex = 4;
            this.label113.Text = "label113";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(114, 49);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(47, 13);
            this.label114.TabIndex = 4;
            this.label114.Text = "label114";
            // 
            // listBox19
            // 
            this.listBox19.FormattingEnabled = true;
            this.listBox19.Location = new System.Drawing.Point(16, 19);
            this.listBox19.Name = "listBox19";
            this.listBox19.Size = new System.Drawing.Size(84, 43);
            this.listBox19.TabIndex = 0;
            // 
            // comboBox19
            // 
            this.comboBox19.FormattingEnabled = true;
            this.comboBox19.Items.AddRange(new object[] {
            "9600",
            "19200",
            "115200"});
            this.comboBox19.Location = new System.Drawing.Point(117, 19);
            this.comboBox19.Name = "comboBox19";
            this.comboBox19.Size = new System.Drawing.Size(99, 21);
            this.comboBox19.TabIndex = 1;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(187, 609);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(47, 13);
            this.label115.TabIndex = 103;
            this.label115.Text = "label115";
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(187, 577);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(47, 13);
            this.label116.TabIndex = 102;
            this.label116.Text = "label116";
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(126, 609);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(47, 13);
            this.label117.TabIndex = 101;
            this.label117.Text = "label117";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(126, 577);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(47, 13);
            this.label118.TabIndex = 100;
            this.label118.Text = "label118";
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(28, 577);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(83, 23);
            this.button20.TabIndex = 99;
            this.button20.Text = "button20";
            this.button20.UseVisualStyleBackColor = true;
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.label119);
            this.groupBox20.Controls.Add(this.label120);
            this.groupBox20.Controls.Add(this.listBox20);
            this.groupBox20.Controls.Add(this.comboBox20);
            this.groupBox20.Location = new System.Drawing.Point(12, 498);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(240, 73);
            this.groupBox20.TabIndex = 98;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "groupBox20";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(175, 49);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(47, 13);
            this.label119.TabIndex = 4;
            this.label119.Text = "label119";
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(114, 49);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(47, 13);
            this.label120.TabIndex = 4;
            this.label120.Text = "label120";
            // 
            // listBox20
            // 
            this.listBox20.FormattingEnabled = true;
            this.listBox20.Location = new System.Drawing.Point(16, 19);
            this.listBox20.Name = "listBox20";
            this.listBox20.Size = new System.Drawing.Size(83, 43);
            this.listBox20.TabIndex = 0;
            // 
            // comboBox20
            // 
            this.comboBox20.FormattingEnabled = true;
            this.comboBox20.Items.AddRange(new object[] {
            "9600",
            "19200",
            "115200"});
            this.comboBox20.Location = new System.Drawing.Point(117, 19);
            this.comboBox20.Name = "comboBox20";
            this.comboBox20.Size = new System.Drawing.Size(99, 21);
            this.comboBox20.TabIndex = 1;
            // 
            // serialPort5
            // 
            this.serialPort5.ReadTimeout = 10;
            this.serialPort5.WriteTimeout = 500;
            // 
            // serialPort6
            // 
            this.serialPort6.ReadTimeout = 10;
            this.serialPort6.WriteTimeout = 500;
            // 
            // serialPort7
            // 
            this.serialPort7.ReadTimeout = 10;
            this.serialPort7.WriteTimeout = 500;
            // 
            // serialPort8
            // 
            this.serialPort8.ReadTimeout = 10;
            this.serialPort8.WriteTimeout = 500;
            // 
            // serialPort9
            // 
            this.serialPort9.ReadTimeout = 10;
            this.serialPort9.WriteTimeout = 500;
            // 
            // serialPort10
            // 
            this.serialPort10.ReadTimeout = 10;
            this.serialPort10.WriteTimeout = 500;
            // 
            // serialPort11
            // 
            this.serialPort11.ReadTimeout = 10;
            this.serialPort11.WriteTimeout = 500;
            // 
            // serialPort12
            // 
            this.serialPort12.ReadTimeout = 10;
            this.serialPort12.WriteTimeout = 500;
            // 
            // serialPort13
            // 
            this.serialPort13.ReadTimeout = 10;
            this.serialPort13.WriteTimeout = 500;
            // 
            // serialPort14
            // 
            this.serialPort14.ReadTimeout = 10;
            this.serialPort14.WriteTimeout = 500;
            // 
            // serialPort15
            // 
            this.serialPort15.ReadTimeout = 10;
            this.serialPort15.WriteTimeout = 500;
            // 
            // serialPort16
            // 
            this.serialPort16.ReadTimeout = 10;
            this.serialPort16.WriteTimeout = 500;
            // 
            // serialPort17
            // 
            this.serialPort17.ReadTimeout = 10;
            this.serialPort17.WriteTimeout = 500;
            // 
            // serialPort18
            // 
            this.serialPort18.ReadTimeout = 10;
            this.serialPort18.WriteTimeout = 500;
            // 
            // serialPort19
            // 
            this.serialPort19.ReadTimeout = 10;
            this.serialPort19.WriteTimeout = 500;
            // 
            // serialPort20
            // 
            this.serialPort20.ReadTimeout = 10;
            this.serialPort20.WriteTimeout = 500;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1003, 642);
            this.Controls.Add(this.label97);
            this.Controls.Add(this.label98);
            this.Controls.Add(this.label99);
            this.Controls.Add(this.label100);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.groupBox17);
            this.Controls.Add(this.label103);
            this.Controls.Add(this.label104);
            this.Controls.Add(this.label105);
            this.Controls.Add(this.label106);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.groupBox18);
            this.Controls.Add(this.label109);
            this.Controls.Add(this.label110);
            this.Controls.Add(this.label111);
            this.Controls.Add(this.label112);
            this.Controls.Add(this.button19);
            this.Controls.Add(this.groupBox19);
            this.Controls.Add(this.label115);
            this.Controls.Add(this.label116);
            this.Controls.Add(this.label117);
            this.Controls.Add(this.label118);
            this.Controls.Add(this.button20);
            this.Controls.Add(this.groupBox20);
            this.Controls.Add(this.label73);
            this.Controls.Add(this.label74);
            this.Controls.Add(this.label75);
            this.Controls.Add(this.label76);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.groupBox13);
            this.Controls.Add(this.label79);
            this.Controls.Add(this.label80);
            this.Controls.Add(this.label81);
            this.Controls.Add(this.label82);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.groupBox14);
            this.Controls.Add(this.label85);
            this.Controls.Add(this.label86);
            this.Controls.Add(this.label87);
            this.Controls.Add(this.label88);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.groupBox15);
            this.Controls.Add(this.label91);
            this.Controls.Add(this.label92);
            this.Controls.Add(this.label93);
            this.Controls.Add(this.label94);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.groupBox16);
            this.Controls.Add(this.label49);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.label51);
            this.Controls.Add(this.label52);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.label55);
            this.Controls.Add(this.label56);
            this.Controls.Add(this.label57);
            this.Controls.Add(this.label58);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.groupBox10);
            this.Controls.Add(this.label61);
            this.Controls.Add(this.label62);
            this.Controls.Add(this.label63);
            this.Controls.Add(this.label64);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.groupBox11);
            this.Controls.Add(this.label67);
            this.Controls.Add(this.label68);
            this.Controls.Add(this.label69);
            this.Controls.Add(this.label70);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.groupBox12);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Request485";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ListBox listBox3;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ListBox listBox4;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.IO.Ports.SerialPort serialPort2;
        private System.IO.Ports.SerialPort serialPort3;
        private System.IO.Ports.SerialPort serialPort4;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ListBox listBox5;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ListBox listBox6;
        private System.Windows.Forms.ComboBox comboBox6;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.ListBox listBox7;
        private System.Windows.Forms.ComboBox comboBox7;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.ListBox listBox8;
        private System.Windows.Forms.ComboBox comboBox8;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.ListBox listBox9;
        private System.Windows.Forms.ComboBox comboBox9;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.ListBox listBox10;
        private System.Windows.Forms.ComboBox comboBox10;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.ListBox listBox11;
        private System.Windows.Forms.ComboBox comboBox11;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.ListBox listBox12;
        private System.Windows.Forms.ComboBox comboBox12;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.ListBox listBox13;
        private System.Windows.Forms.ComboBox comboBox13;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.ListBox listBox14;
        private System.Windows.Forms.ComboBox comboBox14;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.ListBox listBox15;
        private System.Windows.Forms.ComboBox comboBox15;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.ListBox listBox16;
        private System.Windows.Forms.ComboBox comboBox16;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.ListBox listBox17;
        private System.Windows.Forms.ComboBox comboBox17;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.ListBox listBox18;
        private System.Windows.Forms.ComboBox comboBox18;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.ListBox listBox19;
        private System.Windows.Forms.ComboBox comboBox19;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.ListBox listBox20;
        private System.Windows.Forms.ComboBox comboBox20;
        private System.IO.Ports.SerialPort serialPort5;
        private System.IO.Ports.SerialPort serialPort6;
        private System.IO.Ports.SerialPort serialPort7;
        private System.IO.Ports.SerialPort serialPort8;
        private System.IO.Ports.SerialPort serialPort9;
        private System.IO.Ports.SerialPort serialPort10;
        private System.IO.Ports.SerialPort serialPort11;
        private System.IO.Ports.SerialPort serialPort12;
        private System.IO.Ports.SerialPort serialPort13;
        private System.IO.Ports.SerialPort serialPort14;
        private System.IO.Ports.SerialPort serialPort15;
        private System.IO.Ports.SerialPort serialPort16;
        private System.IO.Ports.SerialPort serialPort17;
        private System.IO.Ports.SerialPort serialPort18;
        private System.IO.Ports.SerialPort serialPort19;
        private System.IO.Ports.SerialPort serialPort20;
        public System.Windows.Forms.Timer timer1;
    }
}

